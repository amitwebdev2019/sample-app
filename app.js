
const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors('*'));
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.set('port',process.env.PORT || 6001)

app.get('',async(req,res)=>{
    res.send({status:200,message:'your welcome'})
});

app.get('',async(req,res)=>{
   res.send(process.env);
});

app.get('/item',async(req,res)=>{
  res.send({
    name:'amit patil',
    age:12
  })
});

// epxort
module.exports = app;