
const app = require('./app');

const port = process.env.PORT || 6001;

const server = ()=>{
    app.listen(()=>{
        console.log(`Server is running at http:localhost:${port}`);
    })
}

// start
server();
