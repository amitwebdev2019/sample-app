
.DEFAULT_GOAL=help

install: # To Install Dependancy Packages
	@npm install

test: # To Test Code
	@npm run test 

docker_login:
	@docker login --username=vaishanvi1234 --password=Amit@1234 

build: # Build Docker Image
        @docker build -t vaishanvi1234/test123 .

push: # Push Dokcer Image To Hub
        @docker image push vaishanvi1234/test123

run: # Run Docker Image
        @docker run -d -p 6001:6001 vaishanvi1234/test123


help: # Show this help
	@grep -h '\s#\s' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?# "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'
    


