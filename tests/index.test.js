const request = require( 'supertest' );
const app = require( '../app' );


describe( 'sample test cases', () => {



    it( 'welcome route', async () => {

        const { body, statusCode } = await request( app ).get( '' );
        console.log( body, statusCode );

        expect( body ).toEqual(
            expect.objectContaining( {
                status: expect.any( Number ),
                message: expect.any( String )
            } )
        );
        expect( 200 ).toBe( 200 );

    } );



} );